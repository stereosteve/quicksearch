"use strict";

var QuicksearchDispatcher = require("../dispatcher/QuicksearchDispatcher");
var QuicksearchConstants = require("../constants/QuicksearchConstants");
var EventEmitter = require("events").EventEmitter;
var merge = require("react/lib/merge");
var QueryActions = require("../actions/QueryActions");
var QueryStore = require("./QueryStore");
var _ = require("lodash");
var $ = require("jquery");

var ResponseStore = merge(EventEmitter.prototype, {

  // properties

  _response: null,

  // setters

  _fetchResponse: function() {
    var url = QuicksearchConstants.Config.ENDPOINT + '/api/v3/parts/search?callback=?'
    var params = {
      'q': QueryStore.getQ(),
      'limit': 20,
      'include[]': ['imagesets'],
      'slice[imagesets]': '[:1]',
      'hide[]': ['offers', 'manufacturer', 'uid_v2'],
      apikey: QuicksearchConstants.Config.API_KEY
    }
    var req = $.getJSON(url, params)
    req.done(function(data) {
      console.log('response', data)
      this._response = data
      this.emitChange();
    }.bind(this))
    req.fail(function(err) {
      debugger
    })
  },

  // getters

  getResponse: function() {
    return this._response
  },

  // flux-related

  emitChange: function() {
    this.emit("change");
  },
  addChangeListener: function(callback) {
    this.on("change", callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener("change", callback);
  }
});


var ActionTypes = QuicksearchConstants.ActionTypes;

ResponseStore.dispatchToken = QuicksearchDispatcher.register(function(payload) {
  var action = payload.action;
  switch(action.type) {
    case ActionTypes.SUBMIT_QUERY:
      ResponseStore._fetchResponse();
      break;
    default:
  }
});

module.exports = ResponseStore;
