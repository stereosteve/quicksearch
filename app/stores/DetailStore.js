"use strict";

var QuicksearchDispatcher = require("../dispatcher/QuicksearchDispatcher");
var QuicksearchConstants = require("../constants/QuicksearchConstants");
var EventEmitter = require("events").EventEmitter;
var merge = require("react/lib/merge");
var QueryActions = require("../actions/QueryActions");
var QueryStore = require("./QueryStore");
var _ = require("lodash");
var $ = require("jquery");

var DetailStore = merge(EventEmitter.prototype, {

  // properties

  _part: null,

  // setters

  _fetchDetail: function(uid) {
    var url = QuicksearchConstants.Config.ENDPOINT + '/api/v3/parts/'+uid+'?callback=?'
    var params = {
      apikey: QuicksearchConstants.Config.API_KEY,
      'include[]': ['imagesets', 'datasheets'],
      'slice[imagesets]': '[:1]'
    }
    var req = $.getJSON(url, params)
    req.done(function(data) {
      console.log('detail', data)
      this._part = data
      this.emitChange();
    }.bind(this))
    req.fail(function(err) {
      debugger
    })
  },

  _closeDetail: function () {
    this._part = null;
    this.emitChange();
  },

  // getters

  getPart: function() {
    return this._part
  },

  isCurrentDetail: function(part) {
    return this._part && this._part.uid === part.uid
  },



  // flux-related

  emitChange: function() {
    this.emit("change");
  },
  addChangeListener: function(callback) {
    this.on("change", callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener("change", callback);
  }
});


var ActionTypes = QuicksearchConstants.ActionTypes;

DetailStore.dispatchToken = QuicksearchDispatcher.register(function(payload) {
  var action = payload.action;
  switch(action.type) {
    case ActionTypes.SHOW_DETAIL:
      DetailStore._fetchDetail(action.uid);
      break;
    case ActionTypes.CLOSE_DETAIL:
      DetailStore._closeDetail();
      break;
    default:
  }
});

DetailStore.setMaxListeners(100);

module.exports = DetailStore;
