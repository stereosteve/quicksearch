"use strict";

var QuicksearchDispatcher = require("../dispatcher/QuicksearchDispatcher");
var QuicksearchConstants = require("../constants/QuicksearchConstants");
var EventEmitter = require("events").EventEmitter;
var merge = require("react/lib/merge");
var QueryActions = require("../actions/QueryActions");
var _ = require("lodash");
var $ = require("jquery");

var QueryStore = merge(EventEmitter.prototype, {

  // properties

  _q: "",

  // setters

  _setQ: function(q) {
    this._q = q
  },

  // getters

  getQ: function() {
    return this._q
  },

  // flux-related

  emitChange: function() {
    this.emit("change");
  },
  addChangeListener: function(callback) {
    this.on("change", callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener("change", callback);
  }
});


var ActionTypes = QuicksearchConstants.ActionTypes;

QueryStore.dispatchToken = QuicksearchDispatcher.register(function(payload) {
  var action = payload.action;
  switch(action.type) {
    case ActionTypes.SET_QUERY_PARAM:
      switch(action.key) {
        case "q": QueryStore._setQ(action.value); break;
      }
      QueryStore.emitChange();
      break;
    default:
  }
});

module.exports = QueryStore;
