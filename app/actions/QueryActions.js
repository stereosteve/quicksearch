var QuicksearchDispatcher = require('../dispatcher/QuicksearchDispatcher')
var QuicksearchConstants = require('../constants/QuicksearchConstants')
var ActionTypes = QuicksearchConstants.ActionTypes

var QueryActions = {
  set: function(parameters) {
    QuicksearchDispatcher.handleViewAction({
      type: ActionTypes.SET_QUERY_PARAM,
      key: parameters.key,
      value: parameters.value
    });
  },
  submit: function() {
    QuicksearchDispatcher.handleViewAction({
      type: ActionTypes.SUBMIT_QUERY
    });
  }
};

module.exports = QueryActions;
