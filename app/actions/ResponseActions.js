var QuicksearchDispatcher = require('../dispatcher/QuicksearchDispatcher')
var QuicksearchConstants = require('../constants/QuicksearchConstants')
var ActionTypes = QuicksearchConstants.ActionTypes

var ResponseActions = {
  showDetail: function(parameters) {
    QuicksearchDispatcher.handleViewAction({
      type: ActionTypes.SHOW_DETAIL,
      uid: parameters.uid
    });
  }
};

module.exports = ResponseActions;
