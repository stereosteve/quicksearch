var QuicksearchDispatcher = require('../dispatcher/QuicksearchDispatcher')
var QuicksearchConstants = require('../constants/QuicksearchConstants')
var ActionTypes = QuicksearchConstants.ActionTypes

var DetailActions = {
  close: function() {
    QuicksearchDispatcher.handleViewAction({
      type: ActionTypes.CLOSE_DETAIL
    });
  }
};

module.exports = DetailActions;
