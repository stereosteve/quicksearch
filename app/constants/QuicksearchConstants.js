var keyMirror = require("react/lib/keyMirror");

var QuicksearchConstants = {
  ActionTypes: keyMirror({
    SET_QUERY_PARAM: null,
    SUBMIT_QUERY: null,
    SHOW_DETAIL: null,
    CLOSE_DETAIL: null
  }),
  PayloadSources: keyMirror({
    SERVER_ACTION: null,
    VIEW_ACTION: null
  }),
  Config: {
    API_KEY: "00e73295",
    ENDPOINT: "http://octopart.com"
  }
};

module.exports = QuicksearchConstants;
