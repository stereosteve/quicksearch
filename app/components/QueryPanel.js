/** @jsx React.DOM */

var React = require('react');
var QueryActions = require('../actions/QueryActions')
var QueryStore = require('../stores/QueryStore')

var QueryPanel = React.createClass({
  _getState: function() {
    return {
      q: QueryStore.getQ()
    }
  },
  _onChange: function() {
    this.setState(this._getState());
  },
  _handleQChange: function(ev) {
    QueryActions.set({
      key: 'q',
      value: ev.target.value
    })
  },
  _handleFormSubmit: function(ev) {
    ev.preventDefault();
    QueryActions.submit();
  },
  getInitialState: function() {
    return this._getState();
  },
  componentDidMount: function() {
    QueryStore.addChangeListener(this._onChange);
  },
  componentWillUnmount: function() {
    QueryStore.addChangeListener(this._onChange);
  },
  render: function() {
    return (
      <form className="QueryPanel" onSubmit={this._handleFormSubmit}>
        <input type="text" value={this.state.q} onChange={this._handleQChange} />
        <button>Search</button>
      </form>
    );
  }
});

module.exports = QueryPanel;
