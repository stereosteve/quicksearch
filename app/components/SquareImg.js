/** @jsx React.DOM */

var React = require('react');

var SquareImg = React.createClass({
  propTypes: {
      src: React.PropTypes.string.isRequired,
      size: React.PropTypes.number.isRequired
  },
  render: function () {
    var style = {
      'width': this.props.size,
      'height': this.props.size,
      'background-size': 'contain',
      'background-position': 'center',
      'background-repeat': 'no-repeat',
      'background-image': 'url(' + this.props.src + ')'
    }
    return this.transferPropsTo(<div className="SquareImg" style={style} />)
  }
})


module.exports = SquareImg;
