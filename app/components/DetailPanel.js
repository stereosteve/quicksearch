/** @jsx React.DOM */

var React = require('react');
var DetailStore = require('../stores/DetailStore')
var DetailActions = require('../actions/DetailActions')
var _ = require('lodash')
var formatNumber = require('format-number')()

var DetailPanel = React.createClass({
  _getState: function() {
    return {
      part: DetailStore.getPart()
    }
  },
  _onChange: function() {
    this.setState(this._getState())
  },
  _handleClose: function() {
    DetailActions.close()
  },
  getInitialState: function() {
    return this._getState();
  },
  componentDidMount: function() {
    DetailStore.addChangeListener(this._onChange);
  },
  componentWillUnmount: function() {
    DetailStore.addChangeListener(this._onChange);
  },
  render: function() {
    var part = this.state.part
    if (!part) return null
    var partImg = part.imagesets.length && part.imagesets[0].small_image.url
    var groupedOffers = _.groupBy(part.offers, 'is_authorized')
    return (
      <div className="DetailPanel">
        <button type="button" className="close" onClick={this._handleClose}>
          <span aria-hidden="true">&times;</span>
          <span className="sr-only">Close</span>
        </button>
        <div className="text-center">
          <img src={partImg} />
          <h2>
            {part.brand.name}<br />
            {part.mpn}
          </h2>
        </div>
        <h4>Authorized</h4>
        <OfferTable offers={groupedOffers[true]} />
        <h4>Non-Authorized</h4>
        <OfferTable offers={groupedOffers[false]} />
      </div>
    );
  }

});


//
// OfferTable
//
var OfferTable = React.createClass({
  propTypes: {
    offers: React.PropTypes.array.isRequired
  },
  render: function() {
    var offers = this.props.offers
    return (
      <table className="table table-condensed">
        <thead>
          <tr>
            <th>Seller</th>
            <th className="text-right">Stock</th>
            <th className="text-right">MOQ</th>
          </tr>
        </thead>
        <tbody>
          {offers.map(function(offer) {
            return <OfferRow key={offer.product_url} offer={offer} />
          })}
        </tbody>
      </table>
    )
  }
})


//
// OfferRow
//
var OfferRow = React.createClass({
  propTypes: {
    offer: React.PropTypes.object.isRequired
  },
  render: function() {
    var offer = this.props.offer
    return (
      <tr>
        <td>
          <a href={offer.product_url} target="_blank">
            {offer.seller.name}
          </a>
        </td>
        <td className="text-right">{formatNumber(offer.in_stock_quantity)}</td>
        <td className="text-right">{formatNumber(offer.moq)}</td>
      </tr>
    )
  }
})

module.exports = DetailPanel;
