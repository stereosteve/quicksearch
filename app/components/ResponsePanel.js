/** @jsx React.DOM */

var React = require('react');
var ResponseStore = require('../stores/ResponseStore')
var ResponseActions = require('../actions/ResponseActions')
var DetailStore = require('../stores/DetailStore')
var SquareImg = require('./SquareImg')


var ResponsePanel = React.createClass({
  _getState: function() {
    return {
      response: ResponseStore.getResponse()
    }
  },
  _onChange: function() {
    this.setState(this._getState())
  },
  getInitialState: function() {
    return this._getState();
  },
  componentDidMount: function() {
    ResponseStore.addChangeListener(this._onChange);
  },
  componentWillUnmount: function() {
    ResponseStore.addChangeListener(this._onChange);
  },
  render: function() {
    var response = this.state.response
    var parts
    if (response) {
      parts = _.pluck(response.results, 'item')
    }
    return (
      <div className="ResponsePanel">
        {_.map(parts, function(part) {
          return <PartResult key={part.uid} part={part} />
        })}
      </div>
    );
  }
});


var PartResult = React.createClass({
  propTypes: {
    part: React.PropTypes.object.isRequired
  },
  _handleClick: function() {
    ResponseActions.showDetail({
      uid: this.props.part.uid
    })
  },
  _getState: function() {
    return {
      active: DetailStore.isCurrentDetail(this.props.part)
    }
  },
  _onChange: function() {
    this.setState(this._getState())
  },
  getInitialState: function() {
    return this._getState();
  },
  componentDidMount: function() {
    DetailStore.addChangeListener(this._onChange);
  },
  componentWillUnmount: function() {
    DetailStore.addChangeListener(this._onChange);
  },
  render: function() {
    var part = this.props.part
    var partImg = part.imagesets.length && part.imagesets[0].small_image.url
    var style
    if (this.state.active) {
      style = {
        'background-color': 'lightyellow'
      }
    }
    return (
      <div onClick={this._handleClick} className="PartResult clearfix" style={style}>
        <SquareImg src={partImg} size={60} />
        {part.brand.name}
        <br />
        {part.mpn}
      </div>
    )
  }
})

module.exports = ResponsePanel;
