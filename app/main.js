/** @jsx React.DOM */

var React = require('react')
var QueryPanel = require('./components/QueryPanel')
var ResponsePanel = require('./components/ResponsePanel')
var DetailPanel = require('./components/DetailPanel')

var App = React.createClass({
  render: function() {
    return <div>
      <QueryPanel />
      <ResponsePanel />
      <DetailPanel />
    </div>
  }
})

React.renderComponent(<App />, document.getElementById("App"))
