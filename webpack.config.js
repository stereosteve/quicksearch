module.exports = {
  cache: false,
  entry: './app/main',
  output: {
    filename: 'public/build/build.js'
  },
  module: {
    loaders: [
      {test: /\.js$/, loader: 'jsx-loader'}
    ]
  }
};
